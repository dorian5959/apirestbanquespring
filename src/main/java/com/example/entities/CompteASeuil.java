package com.example.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Ceci est la classe Compte a seuil. <br/>
 * Il n'est pas possible de descendre en dessous du seuil.
 */
@Entity
@DiscriminatorValue("seuil")
public class CompteASeuil extends Compte implements ICompteASeuil {
	private static final long serialVersionUID = 1L;

	private double seuil;

	/**
	 * Constructeur. <br/>
	 * Le seuil par defaut est de 0
	 */
	public CompteASeuil() {
		super();
	}

	/**
	 * Constructeur de l'objet. <br>
	 *
	 * @param unNumero
	 *            le numero du compte
	 * @param unSoldeInitial
	 *            le solde initial du compte
	 * @param unSeuil
	 *            un seuil
	 */
/*	public CompteASeuil(int unNumero, double unSoldeInitial, double unSeuil) {
		super(unNumero, unSoldeInitial);
		this.setSeuil(unSeuil);
	}*/

	@Override
	public double getSeuil() {
		return this.seuil;
	}

	@Override
	public void setSeuil(double unSeuil) {
		this.seuil = unSeuil;
	}

	@Override
	public String toString() {
		String res=" Compte a seuil, Seuil=" + String.valueOf(this.getSeuil());
		for(Operation op: operation) {
			res+= " " + op.toString();
		}
		return res;
	}

	@Override
	public void retirer(double unMontant) throws BanqueException {
		double simu = this.getSolde() - unMontant;
		if (simu <= this.getSeuil()) {
			throw new BanqueException("Votre seuil de " + this.getSeuil() + " ne vous permet pas de retirer "
					+ unMontant + " de votre compte " + this.getId());
		} else {
			super.retirer(unMontant);
		}
	}
}
