package com.example.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;


/**
 * Ceci est la classe Client. <br/>
 *
 * Le client possede comme attributs des types Object ainsi que des types
 * simples. <br/>
 */

@Entity
@Table(name = "utilisateur")
public class Client implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String login;
	private String password;
	private String nom;
	private String prenom;
	@JsonFormat(pattern="yyyy-MM-dd")
	@Column(name = "date_de_naissance")
	private Date dateDeNaissance;
	
	@OneToMany(cascade={CascadeType.PERSIST, CascadeType.REMOVE})
	@JoinColumn(name = "utilisateur_id")
	@ElementCollection
	@MapKey(name="id")
	private Map<Integer, Compte> tabComptes;
	
	
	// Toujours utiliser une interface pour la declaration

	/**
	 * Constructeur de l'objet. <br/>
	 * Par defaut le client aura un numero = -1 et un age de 0
	 */
	public Client() {
		tabComptes = new HashMap<Integer, Compte>();
	}

	/**
	 * Constructeur de l'objet. <br/>
	 *
	 * @param unNumero
	 *            un numero
	 * @param unNom
	 *            le nom du client
	 * @param unPrenom
	 *            le prenom du client
	 * @param unAge
	 *            l'age du client
	 */
//	public Client(int unId, String unNom, String unPrenom, Date uneDate) {
//		super();
//		this.setNom(unNom);
//		this.setPrenom(unPrenom);
//		this.setDateDeNaissance(uneDate);
//		// Par contre, pour l'instanciation, on choisit une classe (HashMap,
//		// HashTable, TreeMap, ...)
//		this.setId(unId);
//	}


	/**
	 * Retourne le nom du client. <br/>
	 *
	 * @return le nom du client
	 */
	public String getNom() {
		return this.nom;
	}

	/**
	 * Retourne le prenom du client. <br/>
	 *
	 * @return le prenom du client
	 */
	public String getPrenom() {
		return this.prenom;
	}



	/**
	 * Fixe le nom du client. <br/>
	 *
	 * @param unNom
	 *            le nouveau nom du client
	 */
	public void setNom(String unNom) {
		this.nom = unNom;
	}
	/**
	 * Fixe le prenom du client. <br/>
	 *
	 * @param unPrenom
	 *            le nouveau prenom du client
	 */
	public void setPrenom(String unPrenom) {
		this.prenom = unPrenom;
	}



	// /**
	// * Pour ceux qui auraient realise une methode setComptes. <br/>
	// *
	// * @param desComptes
	// * une liste de comptes pour le client
	// */
	// public void setComptes(Compte[] desComptes) {
	// // On vide la liste actuelle
	// this.tabComptes.clear();
	// for (Compte unCpt : desComptes) {
	// // On ne met pas dans la liste des cases null du tableau
	// if (unCpt != null) {
	// this.ajouterCompte(unCpt);
	// }
	// }
	// }





	/**
	 * Indique si deux clients sont egaux. <br/>
	 *
	 * Deux clients sont egaux si ils ont le meme numero d'identification.
	 *
	 * @param obj
	 *            l'objet qui sera compare a this
	 * @return <code>true</code> si les deux sont egaux et <code>false</code>
	 *         sinon.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj instanceof Client) {
			Client c = (Client) obj;
			return this.getId() == c.getId();
		}
		return false;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getDateDeNaissance() {
		return dateDeNaissance;
	}

	public void setDateDeNaissance(Date dateDeNaissance) {
		this.dateDeNaissance = dateDeNaissance;
	}

	public int getId() {
		return id;
	}

	public void setId(int id2) {
		this.id = id2;
	}

	@Override
	public String toString() {
		String result="Client [login=" + login + ", password=" + password + ", nom=" + nom + ", prenom=" + prenom
				+ ", dateDeNaissance=" + dateDeNaissance + ", id=" + id + "]";
		
		if(tabComptes!=null) {
			Iterator<Entry<Integer, Compte>> it = tabComptes.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry<Integer, Compte> pair = it.next();
		        result += "\n" + pair.getKey() + " = " + pair.getValue();
		    }
		}
		return result;
	}

	public Map<Integer, Compte> getTabComptes() {
		return tabComptes;
	}

	public void setTabComptes(Map<Integer, Compte> tabComptes) {
		this.tabComptes = tabComptes;
	}
	
	public void  addCompte(Compte compte) {
		this.tabComptes.put(compte.getId(), compte);
	}
	
	public List<Operation> allOperations(){
		List<Operation> operation = new ArrayList<Operation>();

		Iterator<Entry<Integer, Compte>> it = tabComptes.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry<Integer, Compte> pair = it.next();
	        operation.addAll(pair.getValue().getOperation());
	    }
		
		return operation;
		
	}
}