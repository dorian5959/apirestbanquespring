package com.example.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.*;

/**
 * Ceci est la classe Compte. <br>
 */

@Entity
@Table(name = "compte")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
	    name="type",
	    discriminatorType=DiscriminatorType.STRING
	    )
@DiscriminatorValue("basique")
public class Compte implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	@Column(name = "libelle")
	private String libelle;
	@Column(name = "solde")
	private double solde;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "compte", cascade = CascadeType.ALL)
	protected List<Operation> operation;
	
	

	/**
	 * Constructeur de l'objet. <br>
	 * Le numero par defaut sera -1.
	 */
	public Compte() {
		operation = new ArrayList<Operation>();
	}

	/**
	 * Constructeur de l'objet. <br>
	 *
	 * @param unNumero
	 *            le numero du compte
	 * @param unSoldeInitial
	 *            le solde initial du compte
	 */

	/**
	 * Donne acces au solde du compte. <br>
	 *
	 * @return le solde du compte
	 */
	public double getSolde() {
		return this.solde;
	}

	/**
	 * Fixe le solde du compte. <br>
	 *
	 * @param unSolde
	 *            le nouveau solde du compte
	 */
	public void setSolde(double unSolde) {
		this.solde = unSolde;
	}

	/**
	 * Donne acces au numero du compte. <br>
	 *
	 * @return le numero du compte
	 */
	public int getId() {
		return this.id;
	}
	
	public void setId(int id) {
		this.id=id;
	}

	/**
	 * Ajoute un montant au compte. <br>
	 *
	 * @param unMontant
	 *            le montant ajoute au compte
	 */
	public void ajouter(double unMontant) {
		this.setSolde(this.getSolde() + unMontant);
	}

	/**
	 * Retire un montant du compte. <br>
	 *
	 * @param unMontant
	 *            le montant retire du compte
	 * @throws BanqueException
	 *             si un probleme survient
	 */
	public void retirer(double unMontant) throws BanqueException {
		this.setSolde(this.getSolde() - unMontant);
	}



	/**
	 * Recupere le libelle du compte.
	 *
	 * @return le libelle du compte.
	 */
	public String getLibelle() {
		return this.libelle;
	}

	/**
	 * Modifie le libelle du compte.
	 *
	 * @param aLibelle
	 *            le libelle du compte.
	 */
	public void setLibelle(String aLibelle) {
		this.libelle = aLibelle;
	}

	/**
	 * Indique si deux comptes sont egaux. <br>
	 *
	 * Deux comptes sont egaux si ils ont le meme numero d'identification.
	 *
	 * @param obj
	 *            l'objet qui sera compare a this
	 * @return <code>true</code> si les deux sont egaux et <code>false</code>
	 *         sinon.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj instanceof Compte) {
			Compte c = (Compte) obj;
			return this.getId() == c.getId();
		}
		return false;
	}

	@Override
	public String toString() {
		String res="Compte [id=" + id + ", libelle=" + libelle + ", solde=" + solde + "]";
		if(!(this instanceof CompteRemunere) && !(this instanceof CompteASeuil)) {
			for(Operation op: operation) {
				res+= " " + op.toString();
			}
		}
		return res;
	}

	public List<Operation> getOperation() {
		return operation;
	}

	public void setOperation(List<Operation> operation) {
		this.operation = operation;
	}
	
	public void addOperation(Operation operation) {
		this.operation.add(operation);
	}
}
