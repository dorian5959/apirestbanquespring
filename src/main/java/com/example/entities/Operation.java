package com.example.entities;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "operation")
public class Operation {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	private String libelle;
	private double montant;
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date date;
	
	@JoinColumn(name = "compte_id")
    @ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	private Compte compte;
	
	public Operation() {
		
	}
	public Operation(int id, String libelle, double montant, Date date, Compte compte) {
		super();
		this.id = id;
		this.libelle = libelle;
		this.montant = montant;
		this.date = date;
		this.compte = compte;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public double getMontant() {
		return montant;
	}
	public void setMontant(double montant) {
		this.montant = montant;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Compte getCompte() {
		return compte;
	}
	public void setCompte(Compte compte) {
		this.compte = compte;
	}
	
	public void doOperation() {
		compte.setSolde(compte.getSolde()+this.montant);
	}
	
	@Override
	public String toString() {
		return "Operation [id=" + id + ", libelle=" + libelle + ", montant=" + montant + ", date=" + date + "]";
	}
}
