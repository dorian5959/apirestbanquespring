package com.example.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Ceci est la classe Compte remunere. <br>
 */

@Entity
@DiscriminatorValue("remunere")
public class CompteRemunere extends Compte implements ICompteRemunere {
	private static final long serialVersionUID = 1L;

	private double taux;

	/**
	 * Constructeur.
	 */
	public CompteRemunere() {
		super();
	}

	/**
	 * Constructeur de l'objet. <br>
	 *
	 * @param unNumero
	 *            le numero du compte
	 * @param unSoldeInitial
	 *            le solde initial du compte
	 * @param unTaux
	 *            un taux entre [0, 1[
	 */
	/*public CompteRemunere(int unNumero, double unSoldeInitial, double unTaux) {
		super(unNumero, unSoldeInitial);
		this.setTaux(unTaux);
	}*/

	@Override
	public double getTaux() {
		return this.taux;
	}

	@Override
	public void setTaux(double taux) {
		if (taux < 0 || taux >= 1) {
			throw new IllegalArgumentException("Le taux doit etre entre [0, 1[");
		}
		this.taux = taux;
	}

	@Override
	public double calculerInterets() {
		return super.getSolde() * this.getTaux();
	}

	@Override
	public void verserInterets() {
		super.ajouter(this.calculerInterets());
	}

	@Override
	public String toString() {
		String res=" Compte remu, taux=" + String.valueOf(this.getTaux());
		for(Operation op: operation) {
			res+= " " + op.toString();
		}
		return res;
	}

}
