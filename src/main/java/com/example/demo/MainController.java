package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.entities.Client;
import com.example.entities.Compte;
import com.example.entities.Operation;
import com.example.repositories.ClientRepository;
import com.example.repositories.CompteRepository;
import com.example.repositories.OperationRepository;

@CrossOrigin
@RestController
public class MainController {
	
	@Autowired
	private ClientRepository clientRepository;
	@Autowired
	private CompteRepository compteRepository;
	@Autowired
	private OperationRepository operationRepository;
	

	
	@GetMapping("/clients")
    public List<Client> getClients() {
        return (List<Client>) clientRepository.findAll();
    }
 
    @PostMapping("/clients")
    void addClient(@RequestBody Client client) {
    	clientRepository.save(client);
    }
    
    // Single item

    @GetMapping("/clients/{id}")
    Client one(@PathVariable int id) {

      return clientRepository.findById(id);
    }
    
    @PostMapping("/comptes/{clientid}")
    void addCompteToClient(@RequestBody Compte compte, @PathVariable int clientid) {
    	System.out.println(compte.getSolde());
    	compte = this.compteRepository.save(compte);
    	System.out.println(compte.getId());
    	Client client=this.clientRepository.findById(clientid);
    	client.addCompte(compte);
    	clientRepository.save(client);
    }
    
    @GetMapping("/comptes")
    public List<Compte> getComptes() {
        return (List<Compte>) compteRepository.findAll();
    }
 
    @PostMapping("/comptes")
    void addCompte(@RequestBody Compte compte) {
    	compteRepository.save(compte);
    }
    
    @GetMapping("/operations")
    public List<Operation> getOperations() {
        return (List<Operation>) operationRepository.findAll();
    }
    
    @PostMapping("/operations/{compteid}")
    void addOperationToCompte(@RequestBody Operation op, @PathVariable int compteid) {
    	op = this.operationRepository.save(op);
    	Compte compte=this.compteRepository.findById(compteid);
    	op.setCompte(compte);
    	op.doOperation();
    	compte=op.getCompte();
    	compte.addOperation(op);
    	System.out.println(compte.toString());
    	System.out.println(compte.getId());
    	compteRepository.save(compte);
    }
 
    @PostMapping("/operations")
    void addOperation(@RequestBody Operation operation) {
    	operationRepository.save(operation);
    }
    
    
    @PostMapping("/login")
    public boolean login(@RequestBody Client client) {
    	System.out.println("KJKKKKKKKKKKKKKKKZEJRJZERJHZEJRZHJ");
    	ArrayList<Client> cli;
    	cli = this.clientRepository.findByLoginAndPassword(client.getLogin(), client.getPassword()); 
    	for(Client clicli: cli) {
    		System.out.println(clicli);
    	}
        if(cli.size()>0) {
        	return true;
        }
    	
        return false;
    }
     /*
    @RequestMapping("/user")
    public Principal user(HttpServletRequest request) {
        String authToken = request.getHeader("Authorization")
          .substring("Basic".length()).trim();
        return () ->  new String(Base64.getDecoder()
          .decode(authToken)).split(":")[0];
    }*/
}
