package com.example.demo;

import org.apache.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

//(scanBasePackageClasses = {com.example.metier.ClientRepository.class, com.example.metier.CompteRepository.class})


@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
@ComponentScan({"com.example.entities","com.example.demo","com.example.repositories"})
@EntityScan("com.example.entities")
@EnableJpaRepositories("com.example.repositories")

public class TestMapping3Application {
	static org.apache.logging.log4j.Logger logger = LogManager.getLogger(TestMapping3Application.class.getName());
	
	public static void main(String[] args) {
		
		SpringApplication.run(TestMapping3Application.class, args);
	}
}
