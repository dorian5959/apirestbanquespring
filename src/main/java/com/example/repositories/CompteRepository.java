package com.example.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.entities.Compte;

@Repository
public interface CompteRepository extends JpaRepository<Compte, Integer> { // Long: Type of Employee ID.

	Compte findById(int id);

	@Query("SELECT coalesce(max(co.id), 0) FROM Compte co")
	int getMaxId();

	void delete(Compte compteRemunere);
	
}
