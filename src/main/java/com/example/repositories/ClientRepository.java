package com.example.repositories;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.entities.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, Integer> { // Long: Type of Employee ID.

	Client findById(int id);
	ArrayList<Client> findByLoginAndPassword(String login, String password);

	@Query("SELECT coalesce(max(c.id), 0) FROM Client c")
	int getMaxId();

	void delete(Client client);
	
}
