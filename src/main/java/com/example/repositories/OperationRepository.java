package com.example.repositories;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.entities.Operation;

@Repository
public interface OperationRepository extends JpaRepository<Operation, Integer> { // Long: Type of Employee ID.

	Operation findById(int id);

	@Query("SELECT coalesce(max(o.id), 0) FROM Operation o")
	int getMaxId();

	void delete(Operation op);
	
}
